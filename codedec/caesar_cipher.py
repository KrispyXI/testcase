def caesarCipher(s, k):
    letters = list(set(s))
    my_dict_u = {ord(i): (ord(i) - 65 + k) % 26 + 65 for i in letters if i.isupper()}
    my_dict_l = {ord(i): (ord(i) - 97 + k) % 26 + 97 for i in letters if i.islower()}
    s = s.translate(my_dict_u)
    s = s.translate(my_dict_l)
    return(s)