def catAndMouse(catfirst, catsecound, mouse):
    if abs(catfirst - mouse) > abs(catsecound - mouse):
        return('Cat B')
    elif abs(catfirst - mouse) < abs(catsecound - mouse):
        return('Cat A')
    else:
        return('Mouse C')