from codes.two_character import alternate
import unittest

class Test2character(unittest.TestCase):
    def test_2character(self):
        self.assertEqual(alternate('cwomzxmuelmangtosqkgfdqvkzdnxerhravxndvomhbokqmvsfcaddgxgwtpgpqrmeoxvkkjunkbjeyteccpugbkvhljxsshpoymkryydtmfhaogepvbwmypeiqumcibjskmsrpllgbvc'), 8)
    def test_2character2(self):
        self.assertEqual(alternate('ab'), 2)
    def test_2character3(self):
        self.assertEqual(alternate('txnbvnzdvasknhlmcpkbxdvofimsvqbvkswlkrchohwuplfujvlwpxtlcixpajjpaskrnjneelqdbxtiyeianqjqaikbukpicrwpnjvfpzolcredzmfaznnzd'), 6)    
    def test_not2character1(self):
        self.assertEqual(alternate('aaaaa'), 0)
    def test_not2character2(self):
        self.assertEqual(alternate('a'), 0)
        