from codedec.alternating import alternatingCharacters
import unittest

class TestAlternatingcharacter(unittest.TestCase):
    def test_Alter(self):
        self.assertEqual(alternatingCharacters('AAABBBAABB'), 6)
    def test_Alter2(self):
        self.assertEqual(alternatingCharacters('AABBAABB'), 4)
    def test_Alter3(self):
        self.assertEqual(alternatingCharacters('ABABABAA'), 1)