from codedec.catmouse import catAndMouse

import unittest

class CMTest(unittest.TestCase):
    def test_cat_mouse_1(self):
        catfirst = 22
        catsecond = 75
        mouse = 70
        ans = 'Cat B'
        self.assertEqual(catAndMouse(catfirst,catsecond,mouse),ans)

    def test_cat_mouse_2(self):
        catfirst = 33
        catsecond = 86
        mouse = 59
        ans = 'Cat A'
        self.assertEqual(catAndMouse(catfirst,catsecond,mouse),ans)

    def test_cat_mouse_3(self):
        catfirst = 47
        catsecond = 29
        mouse = 89
        ans = 'Cat A'
        self.assertEqual(catAndMouse(catfirst,catsecond,mouse),ans)