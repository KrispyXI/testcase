from codedec.fizz import fizzBuzz

import unittest

class FBtest(unittest.TestCase):
    def test_fizzbuzz1(self):
        in_FB = 11
        ans_FB = 'NoFizzBuzz'
        self.assertEqual(fizzBuzz(in_FB),ans_FB)

    def test_fizzbuzz2(self):
        in_FB = 3
        ans_FB = 'Fizz'
        self.assertEqual(fizzBuzz(in_FB),ans_FB)

    def test_fizzbuzz3(self):
        in_FB = 15
        ans_FB = 'FizzBuzz'
        self.assertEqual(fizzBuzz(in_FB),ans_FB)

    def test_fizzbuzz4(self):
        in_FB = 20
        ans_FB = 'Buzz'
        self.assertEqual(fizzBuzz(in_FB),ans_FB)

