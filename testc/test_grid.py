from codes.grid import gridChallenge
import unittest

class Test_grid(unittest.TestCase):
    def test_Grid(self):
        self.assertEqual(gridChallenge(["eabcd","fghij","olkmn","trpqs","xywuv"]), "Yes")
    def test_Grid2(self):
        self.assertEqual(gridChallenge(["vpvv","pvvv","vzzp","zzyy"]), "Yes")
    def test_Grid3(self):
        self.assertEqual(gridChallenge(["hcd","awc","shm"]), "No")

