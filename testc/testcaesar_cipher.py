from codes.caesar_cipher import caesarCipher
import unittest

class Testcaesar(unittest.TestCase):
    def test_caesar_cipher(self):
        self.assertEqual(caesarCipher('Pz-/aI/J`EvfthGH',3), "Dn-/oW/X`SjthvUV")
    def test_caesar_cipher2(self):
        self.assertEqual(caesarCipher('6DWV95HzxTCHP85dvv3NY2crzt1aO8j6g2zSDvFUiJj6XWDlZvNNr',3), "6MFE95QigCLQY85mee3WH2laic1jX8s6p2iBMeODrSs6GFMuIeWWa")
