from codes.funny import funnyString
import unittest

class Testfunny(unittest.TestCase):
    def test_funnystring(self):
        self.assertEqual(funnyString('abc'), 'Funny')
    def test_funnystring2(self):
        self.assertEqual(funnyString('abcd'), 'Funny')
    def test_funnystring3(self):
        self.assertEqual(funnyString('abcdcba'), 'Funny')    
    def test_notFunnyString1(self):
        self.assertEqual(funnyString('abdca'), 'Not Funny')
        